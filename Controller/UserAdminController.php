<?php

namespace Controller;

use Model\UserModel;

class UserAdminController extends Controller
{

    protected $model;
    
    public function __construct()
    {

        $this->model = new UserModel();

    }
    
    
    public function addUserAction()
    { 
        try 
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {

                $this->model->checkFields(['user_username', 'user_password', 'user_password2'], $_POST);
                $this->model->validatePassword('user_password', 'user_password2', $_POST);
                $this->model->hashPassword('user_password', $_POST);
                $this->model->addUser($_POST);
                $this->model->setMessage('Dodano uzytkownika do bazy danych.', 'success');
            }
        }
        catch (\Exception $ex)
        {
            $exc = $ex->getMessage();
            $this->model->setMessage($exc , 'error');
        }

        $msg = $this->model->displayMessage();
        $this->display('addUser', $data, TRUE, $msg);
    }
    
    public function displayUserAction()
    {
        $data['users'] = $this->model->displayUsers();
        $this->display('displayUsers', $data, TRUE, $msg);
    }
    
    public function editUserAction()
    {
        if(!empty($_GET['id']))
        {
            $data['users'] = $this->model->selectUserById($_GET['id']);
                        
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                try
                {
                    $this->model->checkFields(['user_username', 'user_password', 'user_password2'], $_POST);
                    $this->model->validatePassword('user_password', 'user_password2', $_POST);
                    $this->model->hashPassword('user_password', $_POST);
                    $this->model->updateUser($_GET['id'], $_POST);
                    $this->model->setMessage('Zaktualizowano użytkownika w bazie danych.', 'success');
                } 
                catch (\Exception $ex)
                {
                    $exc = $ex->getMessage();
                    $this->model->setMessage($exc , 'error');
                }
            }
            $msg = $this->model->displayMessage();
            $this->display('editUser', $data, TRUE, $msg);
        }
        else
        {
            header('Location: index.php?controller=ProductAdmin&action=displayProduct');
        }        
        
        
    }
    
    public function deleteUserAction()
    {            
        if(!empty($_GET['id']))
        {
            $this->model->deleteUser($_GET['id']);
            $this->model->setMessage('Usunięto użytkownika z bazy danych.', 'success');
            
            header('Location: index.php?controller=UserAdmin&action=displayUser');
        }
        else
        {
           header('Location: index.php?controller=UserAdmin&action=displayUser'); 
        }    
    }
    
    
    
    
}
