<?php

namespace Controller;

use Model\ProductModel;
use Model\CategoryModel;

class ProductAdminController extends Controller
{

    protected $model;
    
    protected $categoryModel;

    public function __construct() {
        $this->model = new ProductModel();
        $this->categoryModel = new CategoryModel();
    }

    public function addProductAction()
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {

                $this->model->checkFields(['product_title', 'product_brand', 'product_desc', 'product_price', 'product_category'], $_POST);
                $this->model->addProduct($_POST);
                $this->model->setMessage('Dodano produkt do bazy danych.', 'success');
            }
        }
        catch (\Exception $ex)
        {
            $exc = $ex->getMessage();
            $this->model->setMessage($exc , 'error');
        }
        $data['category'] = $this->categoryModel->getCategories();
        $msg = $this->model->displayMessage();
        $this->display('addProduct', $data, TRUE, $msg);
    }

    public function displayProductAction()
    {
        $data['products'] = $this->model->displayProducts();
        $msg = $this->model->displayMessage();
        $this->display('displayProduct', $data, TRUE, $msg);
    }
    
    public function editProductAction()
    {
        if(!empty($_GET['id']))
        {
            $data['products'] = $this->model->selectProductById($_GET['id']);
            $data['category'] = $this->categoryModel->getCategories();
                        
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                try
                {
                    $this->model->checkFields(['product_title', 'product_brand', 'product_desc', 'product_price', 'product_category'], $_POST);
                    $this->model->updateProduct($_GET['id'], $_POST);
                    $this->model->setMessage('Zaktualizowano produkt w bazie danych.', 'success');
                } 
                catch (\Exception $ex)
                {
                    $exc = $ex->getMessage();
                    $this->model->setMessage($exc , 'error');
                }
            }
            $msg = $this->model->displayMessage();
            $this->display('editProduct', $data, TRUE, $msg);
        }
        else
        {
            header('Location: index.php?controller=ProductAdmin&action=displayProduct');
        }
    }
    
    public function deleteProductAction()
    {
        if(!empty($_GET['id']))
        {
            $this->model->deleteProduct($_GET['id']);
            $this->model->setMessage('Usunięto produkt z bazy dancyh.', 'success');
            
            header('Location: index.php?controller=ProductAdmin&action=displayProduct');
        }
        else
        {
           header('Location: index.php?controller=ProductAdmin&action=displayProduct'); 
        }
    }
    
    public function indexAction()
    {
        $this->display('index', $data, TRUE, $msg);
    }

}

