<?php



namespace Controller;

use \Model\Model;

abstract class Controller
{
    
    /**
     * Stala zawierajace separator
     */
    const DS = DIRECTORY_SEPARATOR;

    /**
     * Zmienna zawierajaca sciezke dostepu do widokow
     */
    private $viewsPath = 'Views';

    /**
     * Zmienna zawierajaca sciezke dostepu do widokow - admin
     */
    private $adminPath = 'Views/admin';
    
    /**
     * Zmienna zawierajaca sciezke dostepu do Templates
     * @var type 
     */
    private $templatePath = 'Templates';
    
    /**
     * Zmienna zawierajaca rozszerzenie plikow widoku
     * @var type 
     */
    private $filesExtension = '.html.twig';
    /**
     * Funkcja odpowiadajaca za wczytanie widoku, wyswietlenie danych
     * @param string $views
     * @param type $data
     * @param type $admin
     */
    protected function display($views, $data, $admin = FALSE, $msg = NULL)
    {
        $loader = new \Twig_Loader_Filesystem('Views/');
        $twig = new \Twig_Environment($loader);
       
        if ($admin == TRUE)
        {   
            $admin_files = 'admin';
            $views = $admin_files . self::DS . $views;
            $this->viewsPath = $this->adminPath;
            $this->templatePath = $admin_files . self::DS . $this->templatePath;
            //Dolaczanie pliku header
            echo $twig->render($this->templatePath . self::DS .  'head' . $this->filesExtension, ['PATH' => $this->viewsPath . self::DS]);
        }else
        {
            //echo $twig->render($this->templatePath . self::DS . 'head' . $this->filesExtension, ['PATH' => $this->viewsPath . self::DS]);
        }
      
            echo $twig->render($views . $this->filesExtension, ['data' => $data, 'PATH' => $this->viewsPath . self::DS, 'message' => $msg]);
    }

}
