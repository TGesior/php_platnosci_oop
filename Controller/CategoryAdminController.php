<?php

namespace Controller;

use Model\CategoryModel;

class CategoryAdminController extends Controller
{
    
    protected $model;

    public function __construct()
    {
        $this->model = new CategoryModel();     
    }
    
    public function addCategoryAction()
    {
        try 
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $this->model->checkFields(['category_name', 'category_desc'], $_POST);
                $this->model->addCategory($_POST); 
                $this->model->setMessage('Dodano kategorie do bazy danych.', 'success');
            }
        }
        catch (\Exception $ex)
        {
            $exc = $ex->getMessage();
            $this->model->setMessage($exc , 'error');
        }

        $msg = $this->model->displayMessage();
        $this->display('addCategory', $data, TRUE, $msg);
    }    
    
    public function editCategoryAction()
    {
        if(!empty($_GET['id']))
        {
            $data['category'] = $this->model->selectCategoryById($_GET['id']);
                        
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                try
                {
                    $this->model->checkFields(['category_name', 'category_desc'], $_POST);
                    $this->model->updateCategory($_GET['id'], $_POST);
                    $this->model->setMessage('Zaktualizowano kategorie w bazie danych.', 'success');
                } 
                catch (\Exception $ex)
                {
                    $exc = $ex->getMessage();
                    $this->model->setMessage($exc , 'error');
                }
            }
            $msg = $this->model->displayMessage();
            $this->display('editCategory', $data, TRUE, $msg);
        }
        else
        {
            header('Location: index.php?controller=CategoryAdmin&action=displayCategory');
        }
    }
    
    public function displayCategoryAction()
    {
        $data['categories'] = $this->model->displayCategories();
        $this->display('displayCategories', $data, TRUE, $msg);
    }
    
    public function deleteCategoryAction()
    {
        if(!empty($_GET['id']))
        {
            $this->model->deleteCategory($_GET['id']);
            $this->model->setMessage('Usunięto kategorie z bazy dancyh.', 'success');
            
            header('Location: index.php?controller=CategoryAdmin&action=displayCategory');
        }
        else
        {
           header('Location: index.php?controller=CategoryAdmin&action=displayCategory'); 
        }
    }
}

