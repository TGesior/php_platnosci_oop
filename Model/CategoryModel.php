<?php

namespace Model;

class CategoryModel extends Model
{
    
    protected $db_table;
    
    public function __construct()
    {
        $this->db = new \Model\Model;
        $this->db_table = 'category';
    }

    public function addCategory($categoryData)
    {
        //Zapytanie bazy danych
        $query  = "INSERT INTO " . $this->db_table;
        $query .= '(category_name, category_desc) VALUES (:cat_name,:cat_desc)'; 
        $this->db->prep($query);
        
        $this->db->bind(':cat_name', $categoryData['category_name'] );
        $this->db->bind(':cat_desc', $categoryData['category_desc'] );

        
        $this->db->execute();

    }
    
    public function updateCategory($categoryId, $categoryData)
    {
        $query  = "UPDATE " . $this->db_table . " SET category_name = :cat_name, category_desc = :cat_desc ";
        $query .= " WHERE category_id = " . $categoryId;
        
        $this->db->prep($query);
        
        $this->db->bind(':cat_name', $categoryData['category_name'] );
        $this->db->bind(':cat_desc', $categoryData['category_desc'] );
        
        $this->db->execute();
    }
    
    public function displayCategories()
    {
        $query  = "SELECT * FROM " . $this->db_table;
        $results = $this->db->query($query);
        
        return $results->fetchAll();        
    }
    
    public function deleteCategory($categoryId)
    {
        $query = "DELETE FROM " . $this->db_table . " WHERE category_id = " . $categoryId;
        $this->db->query($query);
    }
    
    public function getCategories()
    {
        $query  = "SELECT category_name FROM " . $this->db_table;
        $results = $this->db->query($query);
        
        return $results->fetch(); 
    }
    
        public function selectCategoryById($categoryId)
    {
        $query  = "SELECT * FROM " . $this->db_table;
        $query .= " WHERE category_id = " . $categoryId;
        $results = $this->db->query($query);
        
        return $results->fetch();
        
    }
}
