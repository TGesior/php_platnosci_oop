<?php

namespace Model;

class Model {

    /**
     * @var type \Core\PDO
     */
    protected $pdo;

    /**
     * @var type Zmienna przechowywujaca zapytanie do bazy danych
     */
    protected $stmt;

    /**
     * Tworzenie polaczenia z baza danych
     */
    protected $message = [];
    
    protected $msgTypes = [
        
        'success' => 'has-succes',
        'waring' => 'has-warning',
        'error'  => 'has-error'   
    ];
    
    public function __construct() {
        $this->pdo = (new \Core\DBConnection())->getConnection();
    }
    
    public function setMessage($message, $type)
    {   
        $type = $this->msgTypes[$type];     
        $this->message[$message] = $type;        
    }
    
    public function displayMessage()
    {
        if(!empty(array_filter($this->message)))
        {
            return $this->message;
            
            unset($this->message);
        }
        
    }
    
    public function checkFields($params = [], $type = NULL)
    {
    foreach ($params as $param)
        {
        if (!empty($type))
            {
            if (empty($type[$param])) 
                {
                throw new \Exception($param . ' nie moze byc puste');
            }
        } 
        else 
            {
            if (empty($param)) 
                {
                throw new \Exception($param . 'nie moze byc puste');
                }
            }
        }
    }   
    
    protected function query($sql)
    {
        return $this->pdo->query($sql);
    }
    /**
     * Przygotowywanie zapytania z baza danych
     * @param type $query
     */
    protected function prep($query) {
        $this->stmt = $this->pdo->prepare($query);
    }

    /**
     * Okreslanie typu zmiennej, przygotowywanie zapytanie do bazy danych
     * @param type $param
     * @param type $value
     * @param type $type
     * @return type \PDO::bindValue
     */
    protected function bind($param, $value, $type = NULL) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                default:
                    $type = \PDO::PARAM_STR;
            }

            $this->stmt->bindValue($param, $value, $type);
        }
    }

    /**
     * Zapytanie bazy danych
     * @return type
     */
    protected function execute() {
        return $this->stmt->execute();
    }

    
}
