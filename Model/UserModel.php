<?php

namespace Model;

class UserModel extends Model
{
    
    protected $db_table;
    
    public function __construct()
    {
        $this->db = new \Model\Model;
        $this->db_table = 'users';
    }
    
    
    /**
     * Dodawanie uzytkownikow do bazy danych
     * @param type $userData
     */
    public function addUser($userData)
    {
        //Zapytanie bazy danych
        $query  = "INSERT INTO " . $this->db_table;
        $query .= '(user_username, user_password) VALUES (:user_username,:user_password)'; 
        $this->db->prep($query);
        
        $this->db->bind(':user_username', $userData['user_username'] );
        $this->db->bind(':user_password', $userData['user_password'] );

        
        $this->db->execute();

    }
    
    public function updateUser($userId, $userData)
    {
        $query  = "UPDATE " . $this->db_table . " SET user_username = :user_username, user_password = :user_password ";
        $query .= "WHERE user_id = " . $userId;
        
        $this->db->prep($query);
        
        $this->db->bind(':user_username', $userData['user_username'] );
        $this->db->bind(':user_password', $userData['user_password'] );
        
        $this->db->execute();
        
    }
    
    public function displayUsers()
    {
        $query  = "SELECT * FROM " . $this->db_table;
        $results = $this->db->query($query);
        
        return $results->fetchAll();        
    }
    
    public function deleteUser($userId)
    {
        $query = "DELETE FROM " . $this->db_table . " WHERE user_id = " . $userId;
        $this->db->query($query);
    }
    
    public function selectUserById($userId)
    {
        $query  = "SELECT * FROM " . $this->db_table;
        $query .= " WHERE user_id = " . $userId;
        $results = $this->db->query($query);
        
        return $results->fetch();
        
    }
    
    public function validatePassword($param, $param2, $type)
    {
        if($type[$param] !== $type[$param2])
        {
            throw new \Exception("Podane hasla sa rozne");
        }
    }
    
   public function hashPassword($param, $type = NULL)
   {
        $type[$param] = password_hash($type[$param], PASSWORD_BCRYPT);
        return $type[$param];
   }
    
    
    
    
    
    
    
}

