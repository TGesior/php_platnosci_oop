<?php

namespace Model;

class ProductModel extends Model
{
    /**
     * @var type Zmienna przechowywujaca nazwe tabeli bazy danych
     */
    private $db_table;
    /**
     *
     * @var type Zmienna polaczenia z baza danych
     */
    private $db;
    /**
     * Tworzenie obiketu rodzica
     */
    public function __construct()
    {
        $this->db = new \Model\Model;
        $this->db_table = 'products';
    }
    
    
    /**
     * Dodawanie produktow do bazy danych
     * @param type $productData
     */
    public function addProduct($productData)
    {
        //Zapytanie bazy danych
        $query  = "INSERT INTO " . $this->db_table;
        $query .= '(product_title, product_price, product_brand, product_desc, product_category)'
                . ' VALUES (:prod_title,:prod_price,:prod_brand,:prod_desc, :prod_cat)'; 
        $this->db->prep($query);
        
        $this->db->bind(':prod_title', $productData['product_title'] );
        $this->db->bind(':prod_price', $productData['product_price'] );
        $this->db->bind(':prod_brand', $productData['product_brand'] );
        $this->db->bind(':prod_desc',  $productData['product_desc'] );
        $this->db->bind(':prod_cat',   $productData['product_category'] );
        
        $this->db->execute();

    }
    
    public function updateProduct($productId, $productData)
    {
        $query  = "UPDATE " . $this->db_table . " SET product_title = :prod_title, product_price = :prod_price, product_brand = :prod_brand,";
        $query .= " product_desc = :prod_desc, product_category = :prod_cat WHERE product_id = " . $productId;
        
        $this->db->prep($query);
        
        $this->db->bind(':prod_title', $productData['product_title'] );
        $this->db->bind(':prod_price', $productData['product_price'] );
        $this->db->bind(':prod_brand', $productData['product_brand'] );
        $this->db->bind(':prod_desc',  $productData['product_desc'] );
        $this->db->bind(':prod_cat',   $productData['product_category'] );
        
        $this->db->execute();
        
    }
    
    public function displayProducts()
    {
        $query  = "SELECT * FROM " . $this->db_table;
        $results = $this->db->query($query);
        
        return $results->fetchAll();        
    }
    
    public function deleteProduct($productId)
    {
        $query = "DELETE FROM " . $this->db_table . " WHERE product_id = " . $productId;
        $this->db->query($query);
    }
    
    public function selectProductById($productId)
    {
        $query  = "SELECT * FROM " . $this->db_table;
        $query .= " WHERE product_id = " . $productId;
        $results = $this->db->query($query);
        
        return $results->fetch();
        
    }
    
    
    public function checkFields($params = [], $type = NULL)
    {
        foreach ($params as $param)
            {
            if (!empty($type))
                {
                if (empty($type[$param])) 
                    {
                    throw new \Exception($param . ' nie moze byc puste');
                }
            } 
            else 
                {
                if (empty($param)) 
                    {
                    throw new \Exception($param . 'nie moze byc puste');
                }
            }
        }
    }

}

