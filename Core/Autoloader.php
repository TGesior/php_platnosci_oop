<?php

namespace Core;

/**
 * Klasa obsluguje automatyczne dolaczanie plikow
 */
class Autoloader
{
    
    
    public function register()
    {
        try{
            spl_autoload_register(array($this,autoload));
            
        } catch (Exception $ex) {
            
            exit("Nie udalo sie wczytac klasy" . $ex);
        }

    }
    
    
    private function autoload($class_name)
    {
       $classPath = str_replace('\\', '/', $class_name);
       
       if(strpos($classPath, 'Twig') == TRUE){
           require_once('vendor/autoload.php');
       }else{
           
           require_once($classPath . ".php");
       }
       
           
       
    }
    
    
    
    
}

