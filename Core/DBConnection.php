<?php

namespace Core;


class DBConnection
{
    
    /**
     * @var Nazwa uzytkownika bazy danych
     */
    const DB_USER = 'phpmaste_plat';
    /**
     * $var Nazwa bazy danych
     */
    const DB_NAME = 'phpmaste_platnosci';
    /**
     * @var Haslo bazy danych
     */
    const DB_PASS = 'kaloryfer';
    /**
     * @var Adres bazy danych
     */
    const DB_HOST = 'localhost';   
    /**     
     * @var type \PDO
     */
    private $pdo;    
    /**
     * Tworzenie polaczenia z baza danych \PDO
     */
    public function __construct()
    {
        
    try{
        /**
         * @var Tworzenie zmiennej DSN \PDO
         */
        $Dsn = "mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_NAME;
        //Tworzenie polaczenia z baza danych
        $this->pdo = new \PDO($Dsn, self::DB_USER, self::DB_PASS);
        //Obsluga wyjatkow polaczenia z baza danych
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        //Zwracanie danych w formie tablicy asocjacyjnej
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        
        } catch (\Exception $ex)
        {
        
        die('Blad polaczenia z baza danych ');
        
        }
        
    }
    
    
    /**
     * 
     * @return type \PDO 
     */
    public function getConnection(){
        return $this->pdo;
    }
    
    
    
    
    
    
    
    
    
}

