<?php

namespace Core;

class App
{
    //Zmienna przechowywujaca nazwe kontrolera
    private $controller;
    //Zmienna przechowywujaca nazwe metody;
    private $action;
    
    public function __construct($params)
    {
        //Sprawdzenie czy jest ustawiona zmienna controller,
        // wywolanie odpowiedniego kontrolera
        if(isset($params['controller']))
        {
            $this->controller = $params['controller'];
        }
        else
        {
            $this->controller = 'Homepage';
        }
        //Sprawdzenie czy jest ustawiona zmienna action,
        // wywolanie odpowieniej metody
        if(isset($params['action']))
        {
            //Wywolanie podstawowej metody
            $this->action = $params['action'];
        }
        else
        {
            //Wywowlanie podstawowej metody
            $this->action = 'index';
        }
        
        
    }
    
    
   public function run()
   {
       //Tworzenie kontrolera
       $controllerName = "\Controller\\" . $this->controller . "Controller";
       //Tworzenie akcji
       $actionName = $this->action . 'Action';
       //Wywolywanie kontrolera
       $controller = new $controllerName();
       //Wywolywanie akcji
       $action = $controller->$actionName();
   }
    
    
    
    
}

